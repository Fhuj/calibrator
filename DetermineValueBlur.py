import cv2
import numpy


class DetermineValueBlur:

    def __init__(self, threshold=197, smoothMatrixSize=4):

        self.__threshold = threshold
        self.__smoothMatrixSize = smoothMatrixSize
        self.__blurValue = 0

    def getThreshold(self):
        return self.__threshold

    def setThreshold(self, value):
        self.__threshold = value

    def getBlurValue(self):
        return self.__blurValue

    def setSmoothMatrixSize(self, value):
        self.__smoothMatrixSize = value

    def getSmoothMatrixSize(self):
        return self.__smoothMatrixSize

    def __cropImage(self, image, startXArea, startYArea, endXArea, endYArea):
        return image[startYArea: endYArea, startXArea: endXArea]

    def __smoothImage(self, image):

        matrixSize = self.__smoothMatrixSize

        for i in range(len(image)):
            for j in range(0, (len(image[i]) // matrixSize) * matrixSize - matrixSize, matrixSize):
                if abs(int(image[i][j]) - int(image[i][j + matrixSize])) > self.__threshold:
                    mean = numpy.mean(image[i][j: j + matrixSize])

                    for k in range(j, j + matrixSize):
                        image[i][k] = mean
        return image

    def __laplaceBlur(self, cropImage):
        laplacianImage = cv2.Laplacian(cropImage, cv2.CV_64F)
        return laplacianImage.var()

    def determineValueBlur(self, image, areaPercent=70, moveX=0, moveY=0):
        areaPercent = areaPercent if 100 > areaPercent > 0 else 70
        areaPercent = areaPercent / 100

        heightImage, widthImage = image.shape[:2]

        startXArea, startYArea = int((widthImage - widthImage * areaPercent) / 2 + moveX), \
                                 int((heightImage - heightImage * areaPercent) / 2 + moveY)
        endXArea, endYArea = int((widthImage + widthImage * areaPercent) / 2 + moveX), \
                             int((heightImage + heightImage * areaPercent) / 2 + moveY)

        cropImage = self.__cropImage(image, startXArea, startYArea, endXArea, endYArea)

        grayCropImage = cv2.cvtColor(cropImage, cv2.COLOR_BGR2GRAY)

        smoothImage = self.__smoothImage(grayCropImage)

        self.__blurValue = self.__laplaceBlur(smoothImage)

        cv2.rectangle(image, (startXArea, startYArea), (endXArea, endYArea), (0, 255, 0), 3)

        cv2.putText(image, "{:.2f}".format(self.__blurValue), (10, 30),
                    cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 4)

        return image, self.__blurValue

    Threshold = property(getThreshold, setThreshold)
    SmoothMatrixSize = property(getSmoothMatrixSize, setSmoothMatrixSize)
